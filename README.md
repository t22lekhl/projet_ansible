# Description du projet: 

Ce projet a pour but de déployer l'application Vapormap sur un host distant via Ansible.

# Clone du projet : 

git clone https://gitlab.imt-atlantique.fr/t22lekhl/projet_ansible.git

# Modifier l'hôte distant par le nom de votre clé SSH dans le fichier host.ini

# Lancement du playbook deploy : 

ansible-playbook -i host.ini deploy.yml

# Lancement du playbook destroy : 

ansible-playbook -i host.ini destroy.yml





